package dk.radoslav.service;

import java.time.LocalDateTime;

import dk.radoslav.model.Meeting;
import dk.radoslav.model.User;

public interface IUsersMeetingsService {

	User createOrUpdateFrom(User user);

	User findUser(String email);

	Meeting createOrUpdateFrom(Meeting meeting);
	
	Meeting findMeeting(LocalDateTime dateTime);

	Meeting[] suggestMeetings(String[] emails);

}
