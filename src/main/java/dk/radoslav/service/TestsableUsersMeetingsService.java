package dk.radoslav.service;

import java.time.LocalDateTime;

import dk.radoslav.Main;

public class TestsableUsersMeetingsService extends UsersMeetingsService{

	@Override
	protected LocalDateTime getDateTimeNow(){
		return Main.MOCKED_DATETIME;
	}
}
