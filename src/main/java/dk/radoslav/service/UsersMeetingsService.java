package dk.radoslav.service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import dk.radoslav.data.InMemoryDatabase;
import dk.radoslav.model.Meeting;
import dk.radoslav.model.User;
import dk.radoslav.util.Utils;

public class UsersMeetingsService implements IUsersMeetingsService{

	public User createOrUpdateFrom(User user) {
		InMemoryDatabase.MapUsers.put(user.getEmail(), user);
		return user;
	}

	public User findUser(String email) {
		User user = InMemoryDatabase.MapUsers.get(email);
		if(user != null){
			List<Meeting> userMeetings = InMemoryDatabase.MapMeetings.values()
					.stream()
					.filter(m -> Arrays.asList(m.getParticipants()).contains(email))
					.collect(Collectors.toList());
			Meeting[] userMeetingsArray = new Meeting[userMeetings.size()];
			user.setMeetings(userMeetings.toArray(userMeetingsArray));
		}

		return user;
	}

	public Meeting createOrUpdateFrom(Meeting meeting) {
		for(String email : meeting.getParticipants()){
			if(!InMemoryDatabase.MapUsers.containsKey(email)){
				throw new RuntimeException("Inconsistent data. This meeting contains users which are not defined in the system.");
			}
		}
		
		InMemoryDatabase.MapMeetings.put(meeting.getTime(), meeting);
		return meeting;
	}
	
	public Meeting findMeeting(LocalDateTime dateTime){
		return InMemoryDatabase.MapMeetings.get(dateTime);
	}

	public Meeting[] suggestMeetings(String[] emails) {
		for(String email : emails){
			if(!InMemoryDatabase.MapUsers.containsKey(email)){
				throw new RuntimeException("Inconsistent data. Some emails are not defined in the system.");
			}
		}
		
		// How many suggestions we will have?
		int numberOfSuggestions = 3;
		Meeting[] suggestedMeetings = new Meeting[numberOfSuggestions];

		// Get the current time and find the fist business hour after.
		LocalDateTime timeNextPossibleMeeting = generateNextBusinessHour(getDateTimeNow());
		
		for(int i = 0; i < numberOfSuggestions; i ++){
			
			// Until found possibility for meeting, try with the next business hour.
			while(!isMeetingPossible(emails, timeNextPossibleMeeting)){
				timeNextPossibleMeeting = generateNextBusinessHour(timeNextPossibleMeeting);
			}
			
			// We found it!
			Meeting meeting = new Meeting();
			meeting.setParticipants(emails);
			meeting.setTime(timeNextPossibleMeeting);
			
			// Add it to the suggestions array.
			suggestedMeetings[i] = meeting;
			
			// Shift the time forward to the next business hour before to try again.
			timeNextPossibleMeeting = generateNextBusinessHour(timeNextPossibleMeeting);
		}
		
		return suggestedMeetings;
	}

	protected LocalDateTime getDateTimeNow() {
		return LocalDateTime.now();
	}

	private boolean isMeetingPossible(String[] emails, LocalDateTime timePossibleMeeting) {
				
		// Is this time/date OK for everyone?
		boolean generatedTimeIsOK = false;
		
		// All meetings in the same time..
		Map<LocalDateTime, Meeting> sameTimeSameUsers = InMemoryDatabase.MapMeetings.entrySet()
			.stream()
			.filter(m -> m.getValue().getTime().isEqual(timePossibleMeeting))
			.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		
		// ..and same (or partially same) participants.
		sameTimeSameUsers = sameTimeSameUsers.entrySet()
			.stream()
			.filter(m -> m.getValue().getParticipants().length > 0 && Utils.isIntersect(m.getValue().getParticipants(), emails))
			.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		
		// No one has meeting at the same time if no results.
		if(sameTimeSameUsers.isEmpty()){
			generatedTimeIsOK = true;
		}
		
		return generatedTimeIsOK;
	}
		
	private LocalDateTime generateNextBusinessHour(LocalDateTime timeNow){
		timeNow = generateBusinessHourFrom(timeNow);
		timeNow = generateBusinessDayFrom(timeNow);
		
		return timeNow;
	}
	
	private LocalDateTime generateBusinessDayFrom(LocalDateTime timeNow){
		if(timeNow.getDayOfWeek().getValue() == 6 ){ // It is Saturday..
			timeNow = timeNow.plusDays(2);
			timeNow = LocalDateTime.of(timeNow.getYear(), timeNow.getMonth(), timeNow.getDayOfMonth(), 9, 0); // Let's try Monday 9 o'clock
		} else if(timeNow.getDayOfWeek().getValue() == 7 ){ // It is Sunday..
			timeNow = timeNow.plusDays(1);
			timeNow = LocalDateTime.of(timeNow.getYear(), timeNow.getMonth(), timeNow.getDayOfMonth(), 9, 0); // Let's try Monday 9 o'clock
		}
		
		return timeNow;
	}
	
	private LocalDateTime generateBusinessHourFrom(LocalDateTime timeNow){
		timeNow = timeNow.plusHours(1);
		
		if(timeNow.getHour() > 16){ // Too late..
			timeNow = timeNow.plusDays(1);  
			timeNow = LocalDateTime.of(timeNow.getYear(), timeNow.getMonth(), timeNow.getDayOfMonth(), 9, 0, 0, 0); // Let's try tomorrow 9 o'clock
		} else if(timeNow.getHour() < 9){ // Too early..
			timeNow = LocalDateTime.of(timeNow.getYear(), timeNow.getMonth(), timeNow.getDayOfMonth(), 9, 0, 0, 0);// Let's try today 9'clock
		}
		
		return LocalDateTime.of(timeNow.getYear(), timeNow.getMonth(), timeNow.getDayOfMonth(), timeNow.getHour(), 0, 0, 0);
	}
	
	
}
