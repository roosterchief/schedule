package dk.radoslav.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Suggestion {
	
	private Meeting[] alternatives;
	
	public Suggestion(){		
	}

	@XmlElement(name="alternatives")
	public Meeting[] getAlternatives() {
		return alternatives;
	}

	public void setAlternatives(Meeting[] alternatives) {
		this.alternatives = alternatives;
	}

}
