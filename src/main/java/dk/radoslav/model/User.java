package dk.radoslav.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User {
	private String name;
	private String email;
	private Meeting[] meetings;

	public User(){
		this.meetings = new Meeting[0];
	}
	
	@XmlElement(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@XmlElement(name="meetings")
	public Meeting[] getMeetings() {
		return meetings;
	}

	public void setMeetings(Meeting[] meetings) {
		this.meetings = meetings;
	}
}
