package dk.radoslav.model;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import dk.radoslav.util.LocalDateTimeAdapter;

import java.time.*;

@XmlRootElement
public class Meeting {

	private String[] participants;
	private LocalDateTime time;

	@XmlElement(name="participants")
	public String[] getParticipants() {
		return participants;
	}

	public void setParticipants(String[] participants) {
		this.participants = participants;
	}
	
	@XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
	@XmlElement(name="time")
	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}
}
