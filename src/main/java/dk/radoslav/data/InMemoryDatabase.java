package dk.radoslav.data;

import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import dk.radoslav.model.Meeting;
import dk.radoslav.model.User;


public class InMemoryDatabase {
	
	/*
	 * NB! Potentially, this storage can lead to memory leaks i.e. blowing the memory when too many records.
	 */
	
	public static final ConcurrentMap<String, User> MapUsers = new ConcurrentHashMap<String, User>();
	public static final ConcurrentMap<LocalDateTime, Meeting> MapMeetings = new ConcurrentHashMap<LocalDateTime, Meeting>();
}