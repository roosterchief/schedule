package dk.radoslav.util;

import java.util.HashSet;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class Utils {

	public static <T> T[] intersect(T[] a, T[] b) {
	    HashSet<T> set1 = new HashSet<T>();
	    for(T i: a){
	        set1.add(i);
	    }
	 
	    HashSet<T> set2 = new HashSet<T>();
	    for(T i: b){
	        if(set1.contains(i)){
	            set2.add(i);
	        }
	    }
	 
	    @SuppressWarnings("unchecked")
		T[] result = (T[])new Object[set2.size()];
	    int i=0;
	    for(T n: set2){
	        result[i++] = n;
	    }
	 
	    return result;
	}
	
	public static <T> boolean isIntersect(T[] a, T[] b) {
	    HashSet<T> set1 = new HashSet<T>();
	    for(T i: a){
	        set1.add(i);
	    }
	 
	    HashSet<T> set2 = new HashSet<T>();
	    for(T i: b){
	        if(set1.contains(i)){
	            set2.add(i);
	        }
	    }
	 
	    @SuppressWarnings("unchecked")
		T[] result = (T[])new Object[set2.size()];
	    int i=0;
	    for(T n: set2){
	        result[i++] = n;
	    }
	 
	    return result.length > 0;
	}
	
	public static boolean isValidEmailAddress(String email) {
		   boolean result = true;
		   try {
		      InternetAddress emailAddr = new InternetAddress(email);
		      emailAddr.validate();
		   } catch (AddressException ex) {
		      result = false;
		   }
		   return result;
		}
}
