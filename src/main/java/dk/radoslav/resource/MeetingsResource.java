package dk.radoslav.resource;

import java.time.LocalDateTime;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import dk.radoslav.model.Meeting;
import dk.radoslav.model.Suggestion;
import dk.radoslav.util.LocalDateTimeAdapter;

@Path("meetings")
public class MeetingsResource extends ResourceBase{
	
	private LocalDateTimeAdapter ldtAdapter = new LocalDateTimeAdapter();
	
	// 3 - create meeting
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMeeting(Meeting meeting, @Context UriInfo uriInfo){
		if(meeting == null || meeting.getParticipants() == null || meeting.getParticipants().length == 0){ // Assume that the date/time is OK and we don't check it...
			return Response.status(Status.BAD_REQUEST).build();
		}
				
		try {
			Meeting createdMeeting = service.createOrUpdateFrom(meeting);
			
			// Generate URI path
			UriBuilder builder = uriInfo.getAbsolutePathBuilder();
			String uriTime = ldtAdapter.marshal(createdMeeting.getTime());
			builder.path(uriTime);
			
			return Response.created(builder.build()).build();	
		} catch (Throwable e) {
			e.printStackTrace();
			
			if(e.getMessage().contains("Inconsistent data.")){
				return Response.status(Status.CONFLICT).build();
			}
			
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}	
	}
	
	// 4 - get meeting
	@GET 
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{uriTime}")
	public Response getUser(@PathParam("uriTime") String uriTime) {
		if(uriTime == null || uriTime.equals("")){
			return Response.status(Status.BAD_REQUEST).build();
		}
				
		try {
			LocalDateTime dateTime = ldtAdapter.unmarshal(uriTime);
			
			Meeting foundMeeting = service.findMeeting(dateTime);
			
			if(foundMeeting == null){
				return Response.status(Status.NOT_FOUND).build();
			}
			
			return Response.ok().entity(foundMeeting).build();
		} catch (Throwable e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}	
	}
	
	// 5 - suggest meetings
	@POST
	@Path("/suggest")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response suggestMeeting(Meeting request){
		if(request == null || request.getParticipants() == null || request.getParticipants().length == 0 || request.getTime() != null){
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		try {
			Meeting[] meetings = service.suggestMeetings(request.getParticipants());
			Suggestion response = new Suggestion();
			response.setAlternatives(meetings);
			
			return Response.ok().entity(response).build();
		} catch (Throwable e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}	
	}
}
