package dk.radoslav.resource;

import dk.radoslav.Main;
import dk.radoslav.service.IUsersMeetingsService;
import dk.radoslav.service.TestsableUsersMeetingsService;
import dk.radoslav.service.UsersMeetingsService;

public class ResourceBase {
	protected IUsersMeetingsService service = Main.UNIT_TESTS_RUNNING ? new TestsableUsersMeetingsService() : new UsersMeetingsService();
}
