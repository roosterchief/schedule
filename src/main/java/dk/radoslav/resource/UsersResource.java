package dk.radoslav.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import dk.radoslav.model.User;
import dk.radoslav.util.Utils;

@Path("users")
public class UsersResource extends ResourceBase{
	
	// 1 - create user
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createUser(User user, @Context UriInfo uriInfo){
		if(user == null || !Utils.isValidEmailAddress(user.getEmail())){
			return Response.status(Status.BAD_REQUEST).build();
		}
				
		try {
			User createdUser = service.createOrUpdateFrom(user);
			
			UriBuilder builder = uriInfo.getAbsolutePathBuilder();
			builder.path(createdUser.getEmail());
			
			return Response.created(builder.build()).build();			
		} catch (Throwable e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}	
	}
	
	// 2 - get user
	@GET 
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{email}")
	public Response getUser(@PathParam("email") String email) {
		if(email == null || email.equals("")){
			return Response.status(Status.BAD_REQUEST).build();
		}
				
		try {
			User foundUser = service.findUser(email);
			
			if(foundUser == null){
				return Response.status(Status.NOT_FOUND).build();
			}
			
			return Response.ok().entity(foundUser).build();
		} catch (Throwable e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}	
	}
}
