package dk.radoslav.client;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.radoslav.model.Meeting;


public class MeetingsClient extends ClientBase {

	public Response create(Meeting meeting) {
		Response response = target.path("meetings")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(meeting, MediaType.APPLICATION_JSON));
		
		return response;
	}
	
	public Response get(String localDateTime) {
		Response response = target.path("meetings/" + localDateTime)
				.request(MediaType.APPLICATION_JSON)
				.get(Response.class);
		
		return response;
	}
	
	public Response suggest(Meeting request) {
		Response response = target.path("meetings/suggest")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(request, MediaType.APPLICATION_JSON));
		
		return response;
	}
}
