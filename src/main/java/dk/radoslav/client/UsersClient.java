package dk.radoslav.client;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.radoslav.model.User;

public class UsersClient extends ClientBase{

	public Response create(User user) {
		Response response = target.path("users")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(user, MediaType.APPLICATION_JSON));
		
		return response;
	}
	
	public Response get(String email) {
		Response response = target.path("users/" + email)
				.request(MediaType.APPLICATION_JSON)
				.get(Response.class);
		
		return response;
	}
}
