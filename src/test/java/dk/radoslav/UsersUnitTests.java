package dk.radoslav;

import java.time.LocalDateTime;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

import org.junit.Test;

import dk.radoslav.client.MeetingsClient;
import dk.radoslav.client.UsersClient;
import dk.radoslav.data.InMemoryDatabase;
import dk.radoslav.model.Meeting;
import dk.radoslav.model.User;

public class UsersUnitTests extends TestBase{

	@Test
	public void testCreateUser_WhenOkInput_ReturnsStatusCode201AndLocation() {
		// Arrange
		UsersClient client = new UsersClient();
		User user = new User();
		user.setEmail("test@test.com");
		user.setName("Tester");
		
		// Act
		Response target = client.create(user);
		
		// Assert
		assertNotNull(target);
		assertEquals(201, target.getStatus());
		String loc = (String)target.getHeaders().getFirst("Location");
		assertTrue(loc.endsWith("/users/test@test.com"));
		
	}
	
	@Test
	public void testCreateUser_WhenInputDataAlreadyExisting_OverwritesExisting() {
		// Arrange
		User alreadyExistingUser = new User();
		alreadyExistingUser.setEmail("test@test.com");
		alreadyExistingUser.setName("TesterOld");
		InMemoryDatabase.MapUsers.put("test@test.com", alreadyExistingUser);
		
		UsersClient client = new UsersClient();
		User user = new User();
		user.setEmail("test@test.com");
		user.setName("TesterNew");
		
		// Act
		Response target = client.create(user);
		
		// Assert
		assertNotNull(target);
		assertEquals(201, target.getStatus());
		String loc = (String)target.getHeaders().getFirst("Location");
		assertTrue(loc.endsWith("/users/test@test.com"));
		assertEquals(1, InMemoryDatabase.MapUsers.entrySet().size());
		assertEquals("TesterNew", InMemoryDatabase.MapUsers.get("test@test.com").getName());
	}
	
	@Test
	public void testCreateUser_WhenBadInput_ReturnsStatusCode400() {
		// Arrange
		UsersClient client = new UsersClient();
		User user = new User();
		user.setEmail("test@...");
		user.setName("Tester");
		
		// Act
		Response target = client.create(user);
		
		// Assert
		assertNotNull(target);
		assertEquals(400, target.getStatus());
	}
	
	@Test
	public void testGetUser_WhenIdentifierExisting_ReturnsStatusCode200AndResource() {
		// Arrange
		UsersClient clientUsers = new UsersClient();
		User user = new User();
		user.setEmail("test@test.com");
		user.setName("Tester");
		clientUsers.create(user);
		
		MeetingsClient clientMeetings = new MeetingsClient();
		Meeting meeting = new Meeting();
		meeting.setTime(LocalDateTime.now());
		meeting.setParticipants(new String[]{"test@test.com"});
		clientMeetings.create(meeting);
		
		// Act
		Response target = clientUsers.get("test@test.com");
		
		// Assert
		assertNotNull(target);
		assertEquals(200, target.getStatus());
		User userResponse = target.readEntity(User.class);
		assertNotNull(userResponse);
		assertEquals(1, userResponse.getMeetings().length);
	}
	
	@Test
	public void testGetUser_WhenNotExisting_ReturnsStatusCode404() {
		// Arrange
		UsersClient clientUsers = new UsersClient();
		User user = new User();
		user.setEmail("test@test.com");
		user.setName("Tester");
		clientUsers.create(user);
		
		MeetingsClient clientMeetings = new MeetingsClient();
		Meeting meeting = new Meeting();
		meeting.setTime(LocalDateTime.now());
		meeting.setParticipants(new String[]{"test@test.com"});
		clientMeetings.create(meeting);
		
		// Act
		Response target = clientUsers.get("test@test1.com");
		
		// Assert
		assertNotNull(target);
		assertEquals(404, target.getStatus());
	}	
}
