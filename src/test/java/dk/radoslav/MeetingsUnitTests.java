package dk.radoslav;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;

import javax.ws.rs.core.Response;

import org.junit.Test;

import dk.radoslav.client.MeetingsClient;
import dk.radoslav.client.UsersClient;
import dk.radoslav.data.InMemoryDatabase;
import dk.radoslav.model.Meeting;
import dk.radoslav.model.Suggestion;
import dk.radoslav.model.User;
import dk.radoslav.util.LocalDateTimeAdapter;

public class MeetingsUnitTests extends TestBase{

	private LocalDateTimeAdapter ldtAdapter = new LocalDateTimeAdapter();
	
	@Test
	public void testCreateMeeting_WhenOkInput_ReturnsStatusCode201AndLocation() {
		// Arrange
		UsersClient client = new UsersClient();
		User user = new User();
		user.setEmail("test@test.com");
		user.setName("Tester");
		client.create(user);
		
		MeetingsClient clientMeetings = new MeetingsClient();
		Meeting meeting = new Meeting();
		LocalDateTime dateTimeNow = LocalDateTime.of(2017, 10, 2, 11, 0, 0); 
		
		String uriTime = "";
		try {
			uriTime = ldtAdapter.marshal(dateTimeNow);
		} catch (Exception e) {
			e.printStackTrace();
		}
		meeting.setTime(dateTimeNow);
		meeting.setParticipants(new String[]{"test@test.com"});
				
		// Act
		Response target = clientMeetings.create(meeting);
		
		// Assert
		assertNotNull(target);
		assertEquals(201, target.getStatus());
		String loc = (String)target.getHeaders().getFirst("Location");
		assertTrue(loc.endsWith("/meetings/" + uriTime));		
	}
	
	@Test
	public void testCreateMeeting_WhenInputDataAlreadyExisting_OverwritesExisting() {
		// Arrange
		User user = new User();
		user.setEmail("test1@test.com");
		user.setName("Tester1");
		InMemoryDatabase.MapUsers.put("test1@test.com", user);
		
		user = new User();
		user.setEmail("test2@test.com");
		user.setName("Tester2");
		InMemoryDatabase.MapUsers.put("test2@test.com", user);
		
		Meeting meeting = new Meeting();
		LocalDateTime dateTimeNow = LocalDateTime.of(2017, 10, 2, 9, 0, 0);
		meeting.setTime(dateTimeNow);
		meeting.setParticipants(new String[]{"test1@test.com"});
		InMemoryDatabase.MapMeetings.put(dateTimeNow, meeting);
		
		meeting = new Meeting();
		dateTimeNow = LocalDateTime.of(2017, 10, 2, 9, 0, 0);
		meeting.setTime(dateTimeNow);
		meeting.setParticipants(new String[]{"test2@test.com"});
		
		MeetingsClient clientMeetings = new MeetingsClient();
		
		// Act
		Response target = clientMeetings.create(meeting);
		
		// Assert
		assertNotNull(target);
		assertEquals(201, target.getStatus());
		String loc = (String)target.getHeaders().getFirst("Location");
		assertTrue(loc.endsWith("/meetings/2017-10-02T09:00:00Z"));
		assertEquals(1, InMemoryDatabase.MapMeetings.entrySet().size());
		String[] participants = InMemoryDatabase.MapMeetings.get(dateTimeNow).getParticipants();
		assertEquals(1, participants.length);
		assertEquals("test2@test.com", participants[0]);
	}

	@Test
	public void testCreateMeeting_WhenBadInputUnknownParticipant_ReturnsStatusCode409() {
		// Arrange
		MeetingsClient client = new MeetingsClient();
		Meeting meeting = new Meeting();
		LocalDateTime dateTimeNow = LocalDateTime.of(2017, 10, 2, 9, 0, 0);
		meeting.setTime(dateTimeNow);
		meeting.setParticipants(new String[]{"test1@test.com"});
		InMemoryDatabase.MapMeetings.put(dateTimeNow, meeting);
		
		// Act
		Response target = client.create(meeting);
		
		// Assert
		assertNotNull(target);
		assertEquals(409, target.getStatus());
	}
	
	@Test
	public void testCreateMeeting_WhenBadInputNoParticipants_ReturnsStatusCode400() {
		// Arrange
		MeetingsClient client = new MeetingsClient();
		Meeting meeting = new Meeting();
		LocalDateTime dateTimeNow = LocalDateTime.of(2017, 10, 2, 9, 0, 0);
		meeting.setTime(dateTimeNow);
		meeting.setParticipants(new String[0]);
		InMemoryDatabase.MapMeetings.put(dateTimeNow, meeting);
		
		// Act
		Response target = client.create(meeting);
		
		// Assert
		assertNotNull(target);
		assertEquals(400, target.getStatus());
	}

	@Test
	public void testGetMeeting_WhenIdentifierExisting_ReturnsStatusCode200AndResource() {
		// Arrange
		UsersClient clientUsers = new UsersClient();
		User user = new User();
		user.setEmail("test@test.com");
		user.setName("Tester");
		clientUsers.create(user);
		
		MeetingsClient clientMeetings = new MeetingsClient();
		Meeting meeting = new Meeting();
		LocalDateTime dateTimeNow = LocalDateTime.of(2017, 10, 2, 14, 0, 0);
		meeting.setTime(dateTimeNow);
		meeting.setParticipants(new String[]{"test@test.com"});
		clientMeetings.create(meeting);
		String meetingId = "";
		try {
			meetingId = ldtAdapter.marshal(dateTimeNow);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Act
		Response target = clientMeetings.get(meetingId);
		
		// Assert
		assertNotNull(target);
		assertEquals(200, target.getStatus());
		Meeting meetingResponse = target.readEntity(Meeting.class);
		assertNotNull(meetingResponse);
		assertEquals(1, meetingResponse.getParticipants().length);
		assertEquals(dateTimeNow, meetingResponse.getTime());
	}
	
	@Test
	public void testGetMeeting_WhenNotExisting_ReturnsStatusCode404() {
		// Arrange
		LocalDateTime dateTimeNow = LocalDateTime.of(2017, 10, 2, 15, 0, 0);
		String meetingId = "";
		try {
			meetingId = ldtAdapter.marshal(dateTimeNow);
		} catch (Exception e) {
			e.printStackTrace();
		}
		MeetingsClient clientMeetings = new MeetingsClient();
		
		// Act
		Response target = clientMeetings.get(meetingId);
		
		// Assert
		assertNotNull(target);
		assertEquals(404, target.getStatus());
	}

	@Test
	public void testScheduleMeeting_When2UsersWithMeetings_Returns3ClosestSuggestionsToNow(){
		// Arrange
		UsersClient client = new UsersClient();
		User user1 = new User();
		user1.setEmail("test1@test.com");
		user1.setName("Tester1");
		client.create(user1);
		
		MeetingsClient clientMeetings = new MeetingsClient();
		Meeting meeting1 = new Meeting();
		LocalDateTime dateTimeNow1 = LocalDateTime.of(2017, 10, 2, 11, 0, 0, 0); 
		meeting1.setTime(dateTimeNow1);
		meeting1.setParticipants(new String[]{"test1@test.com"});
		clientMeetings.create(meeting1);
		
		User user2 = new User();
		user2.setEmail("test2@test.com");
		user2.setName("Tester2");
		client.create(user2);
		
		Meeting meeting2 = new Meeting();
		LocalDateTime dateTimeNow2 = LocalDateTime.of(2017, 10, 2, 12, 0, 0, 0); 
		meeting2.setTime(dateTimeNow2);
		meeting2.setParticipants(new String[]{"test2@test.com"});
		clientMeetings.create(meeting2);
		
		Main.MOCKED_DATETIME = LocalDateTime.of(2017, 10, 1, 10, 15, 0, 0); 
		Meeting request = new Meeting();
		request.setParticipants(new String[]{"test1@test.com", "test2@test.com"});
		
		// Act
		Response target = clientMeetings.suggest(request);
		
		// Assert
		assertNotNull(target);
		assertEquals(200, target.getStatus());
		Suggestion suggestion = target.readEntity(Suggestion.class);
		assertEquals(LocalDateTime.of(2017, 10, 2, 9, 0, 0, 0), suggestion.getAlternatives()[0].getTime());
		assertEquals(LocalDateTime.of(2017, 10, 2, 10, 0, 0, 0), suggestion.getAlternatives()[1].getTime());
		assertEquals(LocalDateTime.of(2017, 10, 2, 13, 0, 0, 0), suggestion.getAlternatives()[2].getTime());
	}
	
	@Test
	public void testScheduleMeeting_When2UsersWithMeetings1UserIrrelevant_Returns3ClosestSuggestionsToNow(){
		// Arrange
		UsersClient client = new UsersClient();
		User user1 = new User();
		user1.setEmail("test1@test.com");
		user1.setName("Tester1");
		client.create(user1);
		
		MeetingsClient clientMeetings = new MeetingsClient();
		Meeting meeting1 = new Meeting();
		LocalDateTime dateTimeNow1 = LocalDateTime.of(2017, 10, 2, 11, 0, 0, 0); 
		meeting1.setTime(dateTimeNow1);
		meeting1.setParticipants(new String[]{"test1@test.com"});
		clientMeetings.create(meeting1);
		
		User user2 = new User();
		user2.setEmail("test2@test.com");
		user2.setName("Tester2");
		client.create(user2);
		
		Meeting meeting2 = new Meeting();
		LocalDateTime dateTimeNow2 = LocalDateTime.of(2017, 10, 2, 12, 0, 0, 0); 
		meeting2.setTime(dateTimeNow2);
		meeting2.setParticipants(new String[]{"test2@test.com"});
		clientMeetings.create(meeting2);
		
		User user3 = new User();
		user3.setEmail("test3@test.com");
		user3.setName("Tester3");
		client.create(user3);
		
		Meeting meeting3 = new Meeting(); // Irrelevant meeting
		LocalDateTime dateTimeNow3 = LocalDateTime.of(2017, 10, 2, 9, 0, 0, 0); 
		meeting3.setTime(dateTimeNow3);
		meeting3.setParticipants(new String[]{"test3@test.com"});
		clientMeetings.create(meeting3);
		
		Main.MOCKED_DATETIME = LocalDateTime.of(2017, 10, 1, 10, 15, 0, 0); 
		Meeting request = new Meeting();
		request.setParticipants(new String[]{"test1@test.com", "test2@test.com"});
		
		// Act
		Response target = clientMeetings.suggest(request);
		
		// Assert
		assertNotNull(target);
		assertEquals(200, target.getStatus());
		Suggestion suggestion = target.readEntity(Suggestion.class);
		assertEquals(LocalDateTime.of(2017, 10, 2, 9, 0, 0, 0), suggestion.getAlternatives()[0].getTime()); // This meeting suggestion will overwrite the irrelevant meeting!
		assertEquals(LocalDateTime.of(2017, 10, 2, 10, 0, 0, 0), suggestion.getAlternatives()[1].getTime());
		assertEquals(LocalDateTime.of(2017, 10, 2, 13, 0, 0, 0), suggestion.getAlternatives()[2].getTime());
	}
	
}
