package dk.radoslav;

import org.glassfish.grizzly.GrizzlyFuture;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;

import dk.radoslav.data.InMemoryDatabase;

public abstract class TestBase {

    private HttpServer server;
    
    @Before
    public void setUp() throws Exception {
    	Main.UNIT_TESTS_RUNNING = true;
        server = Main.startServer();
        InMemoryDatabase.MapUsers.clear();
        InMemoryDatabase.MapMeetings.clear();
    }

    @After
    public void tearDown() throws Exception {
        GrizzlyFuture<HttpServer> future = server.shutdown();
        Thread.sleep(100);
        while(!future.isDone()){
        	Thread.sleep(1000);
        	System.out.println("Not done yet!");
        }
    }
}
