# Documentation

## Purpose
The following document describes the most important points in the solution of the Scheduling App problem:  

_Design and implement the API for a minimal scheduling application. This API should be able to handle the following requirements:_

_1) Create persons with a name and unique email._

_2) Create meetings involving one or more persons at a given time slot._

_3) A meeting can only start at the hour mark and only last exactly one hour._

_4) Show the schedule, i.e., the upcoming meetings, for a given person._

_5) Suggest one or more available timeslots for meetings given a group of persons._

## Used Technologies
The code and the deployed service are using the following technologies:  

1. [Maven](https://maven.apache.org/)
2. [Java 8](http://www.oracle.com/technetwork/java/javaee/downloads/java-ee-sdk-downloads-3908423.html)
3. [JAX-RS / Jersey](https://jersey.github.io/index.html)
4. [Gizzly](https://javaee.github.io/grizzly/)
5. [JUnit](http://junit.org/)
6. [Docker](https://www.docker.com/)
7. [Eclipse](https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/keplersr2) 

## Getting Started
If you want to build and run the solution do the following steps:

1. Download the code from the BitBucket repoistiry at:
```
git clone https://roosterchief@bitbucket.org/roosterchief/schedule.git
```
2. Install the Docker CE (Community Edition) 
```
https://www.docker.com/get-docker
```
3. Build the image for the API by using this command: 
```
docker build -t <docker_username>/<image_name> .
```
4. Run the image
```
docker run -d -p 18080:8080 <docker_username>/<image_name>
```
5. Use CURL requests or some other client to test the solution.

## Architecture
When a request is received by the API, the ```MeetingsResource.java``` or the other "controller" class ```UsersResource.java``` will handle it. These top-level resource-classes use the ```UsersMeetingsService``` which abstracts all operations related to save and load data from the storage - ```ConcurrentHashMap``` (for the users and for the meetings).

Most of the operations will be handled directly in ```UsersMeetingsService.java```, also, the most important part - the alogrithm for suggestions of meetings.

There are also some model classes in the package: ```dk.radoslav.model```. These classes model the incoming requests data, the response data and ensure the entities saved in the datastore. 

## 'Suggest Meeting' Algorithm

Here are the steps which the algorithm will peroform in order to suggest N meetings:

1. Make sure that the provided emails of users who want meeting are exising in the system otherwise stop
2. Based on the current time and day, caclulate the next possible business hour (Monday - Friday, 9 - 16) when it is possible to have a meeting.
3. Make sure that during "the next possible business hour" noone is busy i.e. has already a meeting. (It is OK, if other users, not involved in this request have meeting)
4. Repeat 3. until suitable meeting slot is found (it can be very long if there are a lot of meetings booked already)
5. Repeat N time 4. until number of desired suggesetions for meeting are generated

S. 'Limitations' section for further information about the alogorithm.

## API Endpoints and Returned Status Codes

* ```POST /users``` can return status code 201, 400, 500
* ```GET /users/<email>``` can return status code 200, 404, 500
* ```POST /meetings``` can return status code 201, 400, 500
* ```GET /meetings/<meetingId>``` can return status code 200, 404, 500
* ```POST /meetings/suggest``` can return status code 200, 400, 500


## Unit Tests

There is a minimal amount of unit tests / integration tests available in ```src/test/java``` testing each API endpoint. 

In order to mock the current time it is used the class ```TestsableUsersMeetingsService.java```. Probably, much more cleaner approach will be the use of dependency injection framework.

So, all "hacks" in order to create unit tests without DI framework are in  ```Main.java```:
- ```Main.UNIT_TESTS_RUNNING```
- ```Main.MOCKED_DATETIME```

## Exception Handling

The code has minimal amount of ```try\catch``` blocks. There are 3 places which will do exception handling:

* ```Main.java``` - 1 ocurrance
* ```*Resource.java``` - Handles unexpected scenarios during the processing of REST requests. 
* ```Utils.java``` - 1 occurance. 

## Logging

The code does not use any logging libraray. The handled exceptions will be printed in the console. The only log capability is the log of the Docker container. The stacktraces of the exceptions should be printed there. 

## Limitations

1. Holiday days are not taken into account when the service is calculating suggestions for a meeting. This means it is possible to receive a suggestion for a meeting on Christmas day due to the fact that the day is Monday.

2. It is assumed that the starting day of the working week is Monday and the last day of the working week is Friday. The meetings happens during the working week.

3. Complex concurrency issues for inserting/updating users and meetings are not taken into account. All the operations are not atomic. Overwriting of meetings is acceptable.

4. The system considers only one meeting at the same time, because the date/time is used as identifier (key) in the data storage for meetings.

5. When doing suggestions, however,  it is taken into account if the user is busy or not. E.g. user A has meeting Monday 10.00 o'clock. The system will not suggest him to have meeting at 10.00h but user B will receive a suggestion for this time. Consequently, user B can overwrite the meeting of user A. 

6. It is ussumed that all times are UTC times. When data is sent to the service is assumed it is UTC time. When the service is sending back data it is assumed it is assumed it is in UTC time.
